FROM adoptopenjdk/openjdk8-openj9
COPY airports-assembly-1.0.1.jar   airports-assembly-1.0.1.jar
ENTRYPOINT ["java","-jar","/airports-assembly-1.0.1.jar"]
